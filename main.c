#include <stdio.h>
#include <stdlib.h>
#define A 1000
#define N 10000
#define B 1000000

void decimalToBinary (int n)         // Convert from decimal to binary
{
   int i=0;
   int j=0;
   int tab[N];
   while (n>0)                                  // Loop that divides by 2 and returns the modulo in array tab[i]
   {
       tab[i]=n%2;
       n/=2;
       i++;
   }
   for (int j=i-1; j>=0; j--)                   // Loop that inverts the array tab[i] in the array tab [j]
   {
       printf("%d", tab[j]);
   }
}

void decimalToOctal (int n)          // Convert from decimal to octal
{
   int i=0;
   int j=0;
   int tab[N];
   while (n>0)                                  // Loop that divides by 8 and returns the modulo in the array tab[i]
   {
       tab[i]=n%8;
       n/=8;
       i++;
   }
   for (int j=i-1; j>=0; j--)                   // Loop that inverts the array tab[i] in the array tab [j] and uses the hexadecimal system correspondence
   {
       if (tab[j]==10)
       {
           printf ("A");
       }
       else if (tab[j]==11)
       {
           printf ("B");
       }
       else if (tab[j]==12)
       {
           printf ("C");
       }
       else if (tab[j]==13)
       {
           printf ("D");
       }
       else if (tab[j]==14)
       {
           printf ("E");
       }
       else if (tab[j]==15)
       {
           printf ("F");
       }
       else
       {
           printf ("%d", tab[j]);
       }
   }
}

void decimalToHexadecimal (int n)      // Convert from decimal to hexadecimal
{
   int i=0;
   int j=0;
   int tab[N];
   while (n>0)                                  // Loop that divides by 16 and returns the modulo in the array tab[i]
   {
       tab[i]=n%16;
       n/=16;
       i++;
   }
   for (int j=i-1; j>=0; j--)                   // Loop that inverts the array tab[i] in the array tab [j] and uses the hexadecimal system correspondence
   {
       if (tab[j]==10)
       {
           printf ("A");
       }
       else if (tab[j]==11)
       {
           printf ("B");
       }
       else if (tab[j]==12)
       {
           printf ("C");
       }
       else if (tab[j]==13)
       {
           printf ("D");
       }
       else if (tab[j]==14)
       {
           printf ("E");
       }
       else if (tab[j]==15)
       {
           printf ("F");
       }
       else
       {
           printf ("%d", tab[j]);
       }
   }
}

int charToInt (char str[]) {                // Convert String to Integer
    int l = chainLength(str);
    int num =0;
    for(int i=0; i<l; i++) {
        num*=10;
        num+=str[i]-'0';
    }
    return num;
}

int binaryToDecimal (char str[])            // Convert from binary to decimal
{
    int n = charToInt(str);
    int d=0;                            // d is the decimal
    int r=1;                            // r is the rank

    do
    {
        d+=((n%10)*r);                  // Loop that divides by 10, take the modulo and redivide by 10 to multiply the binary number
        r=2*r;                          // Multiply the rank by 2 with the power, so multiplication by 2 because base 2
        n/=10;
    } while (n!=0);
    return d;                  // returns the decimal number
}

void binaryToOctal (char str[])             // Convert from binary to octal
{
    int n = charToInt(str);
    int d=0;                            // d is the decimal
    int r=1;                            // r is the rank

    do
    {
        d+=((n%10)*r);                  //  Loop that divides by 10, take the modulo and redivide by 10 to multiply the binary number
        r=2*r;                          // Multiply the rank by 2 with the power, so multiplication by 2 because base 2
        n/=10;
    } while (n!=0);
    decimalToOctal(d);
}

int chainLength (const char str[])          // returns the length of the String
{
    int i=0;
    while(str[i]!='\0')
    {
        i++;
    }
    return i;
}

char addZero (char str1[], char str2[])     // add a zero in front of the string str1
{
    int l = chainLength(str1);
    int result;

    for (int i=l-1; i>=0; i--)
    {
        str2[i+1] = str1[i];
    }
    str2 [0] = 48;
    return str2;
}

char numberToBinary (char binary[], char newBinary[])        // add zeros to get bits
{
    int l = chainLength(binary);
    int temp = (4-(l%4));
    int zeroCounterMissing;

    if((l%4) != 0)
    {
        zeroCounterMissing = temp;
        while(zeroCounterMissing != 0)
        {
            addZero(binary, newBinary);   // add zeros in front of binary
            binary = newBinary;             // binary become the new value
            zeroCounterMissing--;
        }
        newBinary[l+4-(l%4)] = '\0';   // finalize the string
    }
    else if ((l%4) == 0)
    {
    int i=0;
        while (binary[i] != '\0') {
            newBinary[i] = binary[i];
            i++;
        }
        newBinary[l+4-(l%4)] = '\0';
    }
    printf("\n %s", newBinary);
    return newBinary;
}

char displayString (char s[]) {                 // display String
    for (int i=0; s[i] != '\0'; i++)
    {
        printf(" %c", s[i]);
    }
    printf("\n");
}

char binaryToHexadecimal (char binary[])            // Convert from binary to hexadecimal
{
    int l = chainLength(binary);
    int temp = (4-(l%4));
    int zeroCounterMissing;
    char newBinary[B];

    if((l%4) != 0)
    {
        zeroCounterMissing = temp;
        while(zeroCounterMissing != 0)
        {
            addZero(binary, newBinary);   // add zeros in front of binary
            binary = newBinary;   // binary become the new value
            zeroCounterMissing--;
        }
        newBinary[l+4-(l%4)] = '\0';   // finalize the string
    }
    else if ((l%4) == 0)
    {
        int m=0;
        while (binary[m] != '\0')
        {
            newBinary[m] = binary[m];
            m++;
        }
        newBinary[l+4-(l%4)] = '\0';
    }

    int s = chainLength(newBinary);
    char tabBin[100];
    char tabHexa[100];
    int k=0, t=0, i=0, j=0;

    while (k<4 || i<s+1)                 // groups the bits by 4 int he array tabBin
    {
        for(int j=i; j<(i+4); j++)
        {
            tabBin[k] = newBinary[j];
            k++;
        }
                                                                                                // uses the hexadecimal system correspondence
        if(tabBin[i]=='0' && tabBin[i+1]=='0' && tabBin[i+2]=='0' && tabBin[i+3]=='0')
        {
            tabHexa[t]= '0';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='0' && tabBin[i+2]=='0' && tabBin[i+3]=='1')
        {
            tabHexa[t]= '1';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='0' && tabBin[i+2]=='1' && tabBin[i+3]=='0')
        {
            tabHexa[t]= '2';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='0' && tabBin[i+2]=='1' && tabBin[i+3]=='1')
        {
           tabHexa[t]= '3';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='1' && tabBin[i+2]=='0' && tabBin[i+3]=='0')
        {
            tabHexa[t]= '4';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='1' && tabBin[i+2]=='0' && tabBin[i+3]=='1')
        {
            tabHexa[t]= '5';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='1' && tabBin[i+2]=='1' && tabBin[i+3]=='0')
        {
            tabHexa[t]= '6';
            t++;
        }
        if(tabBin[i]=='0' && tabBin[i+1]=='1' && tabBin[i+2]=='1' && tabBin[i+3]=='1')
        {
            tabHexa[t]= '7';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='0' && tabBin[i+2]=='0' && tabBin[i+3]=='0')
        {
            tabHexa[t]= '8';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='0' && tabBin[i+2]=='0' && tabBin[i+3]=='1')
        {
            tabHexa[t]= '9';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='0' && tabBin[i+2]=='1' && tabBin[i+3]=='0')
        {
            tabHexa[t]= 'A';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='0' && tabBin[i+2]=='1' && tabBin[i+3]=='1')
        {
           tabHexa[t]= 'B';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='1' && tabBin[i+2]=='0' && tabBin[i+3]=='0')
        {
            tabHexa[t]= 'C';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='1' && tabBin[i+2]=='0' && tabBin[i+3]=='1')
        {
            tabHexa[t]= 'D';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='1' && tabBin[i+2]=='1' && tabBin[i+3]=='0')
        {
            tabHexa[t]= 'E';
            t++;
        }
        if(tabBin[i]=='1' && tabBin[i+1]=='1' && tabBin[i+2]=='1' && tabBin[i+3]=='1')
        {
            tabHexa[t]= 'F';
            t++;

        }
        i+=4;
    }
    tabHexa[t+1] = '\0';
    displayString(tabHexa);
    return tabHexa;
}

int hexadecimalToDecimal (char hexadecimal[])           // Convert from hexadecimal to decimal
{
    int l = chainLength(hexadecimal);
    int r = 1;
    int d = 0;
    int i = 0;

    for(i=l-1; i>=0; i--)
    {
        if (hexadecimal[i]>='0' && hexadecimal[i]<='9')
        {
            d += (hexadecimal[i] - 48)*r;
            r = r*16;
        }
        else if (hexadecimal[i]>='A' && hexadecimal[i]<='F')
        {
            d += (hexadecimal[i] - 55)*r;
            r = r*16;
        }
    }
    return d;
}

void hexadecimalToBinary (char hexadecimal[])           // Convert from hexadecimal to binary
{
   for(int i=0; hexadecimal[i] != '\0'; i++)
    {
       if (hexadecimal[i]=='0')
       {
           printf ("0000");
       }
       else if (hexadecimal[i]=='1')
       {
           printf ("0001");
       }
       else if (hexadecimal[i]=='2')
       {
           printf ("0010");
       }
       else if (hexadecimal[i]=='3')
       {
           printf ("0011");
       }
       else if (hexadecimal[i]=='4')
       {
           printf ("0100");
       }
       else if (hexadecimal[i]=='5')
       {
           printf ("0101");
       }
       else if (hexadecimal[i]=='6')
       {
           printf ("0110");
       }
       else if (hexadecimal[i]=='7')
       {
           printf ("0111");
       }
       else if (hexadecimal[i]=='8')
       {
           printf ("1000");
       }
       else if (hexadecimal[i]=='9')
       {
           printf ("1001");
       }
       else if (hexadecimal[i]=='A')
       {
           printf ("1010");
       }
       else if (hexadecimal[i]=='B')
       {
           printf ("1011");
       }
       else if (hexadecimal[i]=='C')
       {
           printf ("1100");
       }
       else if (hexadecimal[i]=='D')
       {
           printf ("1101");
       }
       else if (hexadecimal[i]=='E')
       {
           printf ("1110");
       }
       else if (hexadecimal[i]=='F')
       {
           printf ("1111");
       }

    }

}

void hexadecimalToOctal (char hexadecimal[])            // Convert from hexadecimal to octal
{
    int intermediaire = hexadecimalToDecimal(hexadecimal);
    decimalToOctal(intermediaire);
}



int main()
{
    char fin = 'n';

    do
    {
        int n;
        char s[50];
        char str[N];
        int tab[N];
        char r[B];

        printf ("CONVERTISSEUR DE BASES\n\n");
        printf ("Choississez la base %c convertir : \n\nTapez \"D\" pour du d%ccimal \nTapez \"B\" pour du binaire (commencer par le premier 1) \nTapez \"H\" pour de l'hexad%ccimal\n", 133, 130, 130);
        char choose='B';
        scanf (" %c", &choose);


        if (choose == 'D')
        {
            printf("Donnez votre base : ");
            scanf ("%d", &n);

            printf ("Le nombre d%ccimal %d correspond : \n",130, n);
            printf ("en binaire %c : ", 133);
            decimalToBinary(n);
            printf("\n");
            printf ("en octal %c : ", 133);
            decimalToOctal(n);
            printf("\n");
            printf ("en h%cxad%ccimal %c : ", 130, 130, 133);
            decimalToHexadecimal(n);
        }
        else if (choose == 'B')
        {
            printf("Donnez votre base : ");
            scanf (" %s", &s);

            printf ("Le nombre binaire %s correspond : \n", s);
            printf ("en d%ccimal %c : ", 130, 133);
            int binToDec = binaryToDecimal(s);
            printf (" %d", binToDec);
            printf("\n");
            printf ("en octal %c : ", 133);
            binaryToOctal(s);
            printf("\n");
            printf ("en h%cxad%ccimal %c : ", 130, 130, 133);
            binaryToHexadecimal(s);

        }
        else if (choose == 'H')
        {
            printf("Donnez votre base : ");
            scanf (" %s", &str);

            printf ("Le nombre h%cxad%ccimal %s correspond %c : \n", 130, 130, str, 133);
            printf ("en d%ccimal %c : ", 130, 133);
            int hexaToDec = hexadecimalToDecimal(str);
            printf("%d", hexaToDec);
            printf ("\n");
            printf ("en octal %c : ", 133);
            hexadecimalToOctal(str);
            printf ("\n");
            printf ("en binaire %c : ", 133);
            hexadecimalToBinary(str);
        }
        printf ("\n");
        printf ("Voulez-vous convertir une autre base ?\n");
        scanf (" %c", &fin);
    }
    while (fin != 'n');

    return 0;
}
